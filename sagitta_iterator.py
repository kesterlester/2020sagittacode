#!/usr/bin/python
'''
hpsiuhdfouihd
'''

from ROOT import *
gROOT.SetBatch(1)
gStyle.SetOptStat(0)

from CommonTools import rand_uuid, myText, ATLASLabel
import math 

class Hist():

   def __init__(self, xbins, xmin, xmax, ybins, ymin, ymax):
      self.value_hist = TH2D("vals"+rand_uuid(), "", xbins, xmin, xmax, ybins, ymin, ymax)
      self.counts_hist = TH2D("counts"+rand_uuid(), "", xbins, xmin, xmax, ybins, ymin, ymax)

   def fill(self, xpos, ypos, val):
      self.value_hist.Fill(xpos, ypos, val)
      self.counts_hist.Fill(xpos, ypos)

   def combine_averages(self, hist):
      '''
      Return the average of two Hist objects 
      '''
      total_values = self.value_hist.Clone()
      total_values.Add(hist.value_hist)

      total_counts = self.counts_hist.Clone()
      total_counts.Add(hist.counts_hist)

      average = total_values.Clone()
      average.Divide(total_counts) 

      return average 



def main(debug):

   can = TCanvas("can", "can", 500, 500) 

   n_iterations = 50
   sagitta_averages = None
   for niter in range(1, n_iterations+1):
      plus_hist, minus_hist, simple_plus, simple_minus = iteration(sagitta_averages)

      new_sagitta_averages = plus_hist.combine_averages(minus_hist) 

      # Stop condition
      if niter>2:
         pass
      sagitta_averages = new_sagitta_averages


      make_plots(t
      #######################
      # Plot drawing and style 
      #######################
      plus_colour = 2
      minus_colour = 4
      # 2D average plot 
      can.SetRightMargin(0.2)
      sagitta_averages.GetXaxis().SetTitle("Eta")
      sagitta_averages.GetYaxis().SetTitle("Phi")
      sagitta_averages.Draw("colz")
      zaxis =  sagitta_averages.GetZaxis()
      zaxis.SetTitle("#LT #delta_{sagitta} #GT [GeV^{#minus 1}]")
      zaxis.SetTitleOffset(2.0)
      ATLASLabel(0.1, 0.91, "Internal")
      myText(0.5, 0.91, "Iteration: {0}".format(niter))
      can.SaveAs("average_sagitta_{0}.pdf".format(niter))

      # 1D plot 
      can.SetRightMargin(0.1)
      simple_plus.GetXaxis().SetTitle('#delta_{sagitta} [GeV^{#minus1}]')
      simple_plus.SetMarkerStyle(20)
      simple_plus.SetMarkerColor(plus_colour)
      simple_plus.SetLineColor(plus_colour)
      simple_plus.SetMarkerSize(0.5)
      simple_plus.DrawNormalized("E0")
      plus_mean = simple_plus.GetMean()
      plus_stddev = simple_plus.GetStdDev()

      simple_minus.SetMarkerStyle(22)
      simple_minus.SetMarkerSize(0.5)
      simple_minus.SetMarkerColor(minus_colour)
      simple_minus.SetLineColor(minus_colour)
      simple_minus.DrawNormalized("Same")
      minus_mean = simple_minus.GetMean()
      minus_stddev = simple_minus.GetStdDev()

      both = simple_plus.Clone()
      both.Add(simple_minus)
      both.SetMarkerColor(1)
      both.SetLineColor(1)
      both.DrawNormalized("same")
      both_mean = both.GetMean()
      both_stddev = both.GetStdDev()

      leg = TLegend(0.15, 0.75, 0.45, 0.90)
      leg.SetTextFont(42)
      leg.AddEntry(simple_plus, "#mu^{+} : "+"{0:.2e}, {1:.3e}".format(plus_mean, plus_stddev), "lp")
      leg.AddEntry(simple_minus,"#mu^{#minus} : "+"{0:.2e}, {1:.3e}".format(minus_mean, minus_stddev), "lp")
      leg.AddEntry(both, "both : {0:.2e}, {1:.3e}".format(both_mean, both_stddev), "lp")
      leg.SetFillStyle(0)
      leg.SetBorderSize(0)
      leg.Draw()

      ATLASLabel(0.1, 0.91, "Internal")
      myText(0.5, 0.91, "Iteration: {0}".format(niter))

      can.SaveAs("sagitta_simple_{0}.pdf".format(niter))



def iteration(sagitta_averages):
   TeV = 1000.0
   mZ = 91.187 
   mZ2 = mZ**2 

   # eta
   xbins = 50
   xmin = -2.4
   xmax = 2.4
   # phi  
   ybins = 50
   ymin = -1*math.pi
   ymax = math.pi

   # Create objects to store the binned sagitta results 
   plus_hist  = Hist(xbins, xmin, xmax, ybins, ymin, ymax)
   minus_hist = Hist(xbins, xmin, xmax, ybins, ymin, ymax)

   # 1D histograms to store simple sagitta  
   if sagitta_averages is None:
      axis_range = 0.004 
   else:
      axis_range = 0.00004  
   simple_sagitta_plus = TH1D("sagitta"+rand_uuid(), "", 100, -1*axis_range, axis_range)
   simple_sagitta_plus.Sumw2()
   simple_sagitta_minus = TH1D("sagitta"+rand_uuid(), "", 100, -1*axis_range, axis_range)
   simple_sagitta_minus.Sumw2()

   ifile_path = "/usera/wfawcett/cambridge/emus/OSDFChargeFlavourAsymmCode/cutflows/run/test_data_muons.root"
   ifile_path = "/r10/atlas/emus/histograms/data17Aug_withMuons_2008111409_merged/data15.muons.root"
   ifile = TFile.Open(ifile_path, "read")
   # Loop over all events  
   for event in ifile.tree:



      # mu+
      pt_plus = event.muPlusPt #/ TeV 
      eta_plus = event.muPlusEta
      phi_plus = event.muPlusPhi
      # Extract the saggita average from the previous iteration
      try:
         old_sagitta_plus = sagitta_averages.GetBinContent(sagitta_averages.GetXaxis().FindBin(eta_plus), sagitta_averages.GetYaxis().FindBin(phi_plus) )
      except AttributeError:
         # Deal with first iteration  
         old_sagitta_plus = 0 

   
      # mu-
      pt_minus = event.muMinusPt# / TeV
      eta_minus = event.muMinusEta
      phi_minus = event.muMinusPhi
      # Extract the saggita average from the previous iteration
      try:
         old_sagitta_minus = sagitta_averages.GetBinContent(sagitta_averages.GetXaxis().FindBin(eta_minus), sagitta_averages.GetYaxis().FindBin(phi_minus) )
      except AttributeError:
         # Deal with first iteration  
         old_sagitta_minus = 0 

      # Calculate the invariant mass squared 
      if sagitta_averages is None:
         # First iteration, extract this from the ntuple
         mll = event.mll 
         mll2 = mll**2 
      else:
         # Subsequent iterations, recalculate 
         mll2 = mZ2 + mZ2*( pt_plus * old_sagitta_plus - pt_minus * old_sagitta_minus)

      # Calculate the sagittas for this event 
      sagitta_plus  = calc_sagitta( 1, mll2, mZ2, pt_plus,  old_sagitta_plus) 
      sagitta_minus = calc_sagitta(-1, mll2, mZ2, pt_minus, old_sagitta_minus)

      plus_hist.fill(eta_plus, phi_plus, sagitta_plus)
      minus_hist.fill(eta_minus, phi_minus, sagitta_minus) 

      simple_sagitta_plus.Fill(sagitta_plus)
      simple_sagitta_minus.Fill(sagitta_minus)

   ifile.Close() 

   return plus_hist, minus_hist, simple_sagitta_plus, simple_sagitta_minus 

def calc_sagitta(charge, mll2, mZ2, pt, old_sagitta):
   return  (-1*charge * (mll2 - mZ2)/(2*mZ2) * (1 + charge * pt * old_sagitta) / pt)  + old_sagitta


if __name__ == "__main__":

   import argparse
   parser = argparse.ArgumentParser()
   parser.add_argument("-d", "--debug", help="Turn on debug messages", action="store_true", default=False)
   args = parser.parse_args()
   debug = args.debug

   main(debug)
