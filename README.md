# Compilation
Compilation uses CMake, and ROOT, first make sure you have access to these, e.g.:
```
setupATLAS
lsetup "root 6.18.04"
lsetup cmake
```
Then, to compile
```
cd build
cmake ..
make 
```
If this is your first time compiling you will need to make two directories:
```
mkdir build bin
```
After succeesful compilation, executables are stored in the `bin` directory. To run the code, do:
```
./bin/iterator
```
for example. 

