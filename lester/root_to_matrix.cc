#include <iomanip>
#include <iostream>
#include <vector>
#include <fstream>
#include <cmath>
#include <sstream>
#include <Eigen/Dense>


const double TeV = 1; // By making TeV=1, all our outputs are TeV based.
const double GeV = 0.001*TeV;
const double Z_mass = 91.19*GeV; 



struct Bins {
   int nCore; // not including any under or overflow bins
   int nUnderflow;
   int nOverflow;
   int nTot; // including any under or overflow bins
   double l;
   double u;
   double factor;
   double inverseFactor;
   Bins(const int n /*not including any overflow or underflow bins*/, 
		   const double l, 
		   const double u, 
		   bool useUnderflowBool, 
		   bool useOverflowBool) :
	   nCore(n), 
	   nUnderflow(useUnderflowBool ? 1 : 0),
	   nOverflow(useOverflowBool ? 1 : 0),
	   nTot(n + nUnderflow + nOverflow), 
	   l(l), u(u), 
	   factor(static_cast<double>(nCore)/(u-l)),
	   inverseFactor((u-l)/static_cast<double>(nCore)) {
   }
   double unit_frac(double x) const { return (x-l)/(u-l); }
   double pos(int bin, const double fracWithinBin) const {
	   const double dist = fracWithinBin+static_cast<double>(bin - nUnderflow);
	   return dist*inverseFactor + l;
   }
   double lower   (int bin) const { return pos(bin, 0.0); }
   double centroid(int bin) const { return pos(bin, 0.5); }
   double upper   (int bin) const { return pos(bin, 1.0); }
   std::string histAxisSpec(const std::string x_y_or_z) const {
       std::ostringstream o;
       o
           << "n" << x_y_or_z << " " << nTot << " "
           << "l" << x_y_or_z << " " << lower(0) << " "
           << "u" << x_y_or_z << " " << upper(nTot-1);
       return o.str();
   }
   int find_bin(const double x) const {
     int tmp = floor((x-l)*factor);
     if (tmp<0) {
	    if (nUnderflow) {
	      tmp=-1;
	    } else {
	      std::cerr << "Bin Underflow!" << std::endl;
	      std::cerr << "nTot " << nTot << ", l " << l << ", u " << u << ", x " << x << std::endl;
	      exit(1);
	    }
     }
     if (tmp>=nCore) {
	    if (nOverflow) {
              tmp=nCore;
	    } else {
	      std::cerr << "Bin Overflow!" << std::endl;
	      std::cerr << "nTot " << nTot << ", l " << l << ", u " << u << ", x " << x << std::endl;
	      exit(2);
	    }
     }
     tmp += nUnderflow;
     if (tmp<0 || tmp>=nTot) {
	      std::cerr << "Bin calc broken!" << std::endl;
	      std::cerr << "nTot " << nTot << ", l " << l << ", u " << u << ", x " << x << ", tmp" << tmp << std::endl;
	      exit(3);
     }
     return tmp;
   }
};

double doubly_unit_smiley_centred(const double x, const double y) {
 const double r=sqrt(x*x+y*y);
 const double theta = atan2(x,y);
 const int checker10 = 1;//2*(((int)floor(10 + 10*theta/(2.*M_PI)))%2)-1; 
 if (!(checker10==1 || checker10==-1)) {
	 std::cerr<< "Bad checker10" << std::endl;
	 exit(10);
 }
 if (r>0.85 & r<0.95) return checker10; // head
 if (r>0.35 & r<0.55) {
	 if (fabs(theta)>0.8 && fabs(theta)<1.1) return 1; // eyes
 }
 if (r>0.4 & r<0.5) {
	 if (fabs(theta)>2) return checker10; // smile
 }
 return 0; // everything else.
}

double unit_smiley_first_quadrant(const double x, const double y) {
  return doubly_unit_smiley_centred(2*x-1,2*y-1);
}

struct LVec {
 double E;
 double px;
 double py;
 double pz;
 LVec() : E(0), px(0), py(0), pz(0) {};
 LVec(double E, double px, double py, double pz) : E(E), px(px), py(py), pz(pz) {};
 LVec operator+(const LVec & other) {
	 return LVec(E+other.E, px+other.px, py+other.py, pz+other.pz);
 }
 double m2() const { return E*E-(px*px+py*py+pz*pz); }
 void set_m_pt_eta_phi(
		 const double m, 
		 const double pt, 
		 const double eta, 
		 const double phi) {
    px = pt*cos(phi);
    py = pt*sin(phi);
    pz = pt*sinh(eta);
    const double pSq = px*px + py*py + pz*pz;
    E=sqrt(pSq + m*m);
 }
};

std::ostream & operator<<(std::ostream & os, const LVec & v) {
 return os << "LV["<<v.E 
	 <<", "<<v.px
	 <<", "<<v.py
	 <<", "<<v.pz
	 << "]";
}

struct Lepton {
  // old thing was (charge * |pT| * mll2)
  // new thing is (charge * |pT|)
  double charge;
  double pT; // By definintion positive ... not charge-signed pT!
  double new_thing() const { return charge*pT; }
  double eta;
  double phi;
  LVec lvec() const {
  	LVec lep;
	lep.set_m_pt_eta_phi(0, pT, eta, phi);
	return lep;
  }
};

struct Event {
  double mll2;
  std::vector<Lepton> leps;
  Event() : leps(2) {
  }
};

struct Bins2D {
   Bins xBins;
   Bins yBins;
   unsigned int bins;
   Bins2D(const Bins & xBins, const Bins & yBins) : xBins(xBins), yBins(yBins), bins(xBins.nTot * yBins.nTot) {};
   int find_bin(const double eta, const double phi) const {
     const int xBin = xBins.find_bin(eta);
     const int yBin = yBins.find_bin(phi);
     const int tmp = xBin + xBins.nTot * yBin;
     if (tmp<0 || tmp>=bins) {
   	      std::cerr << "Leptonic double Bin calc broken!" << std::endl;
   	      exit(4);
     }
     return tmp;
   }
   std::string histAxisSpec() const { return xBins.histAxisSpec("x")+" "+yBins.histAxisSpec("y"); }
   struct BinCentroid {
   	double eta;
   	double phi;
   	BinCentroid(double eta, double phi) : eta(eta), phi(phi) {};
   };
   const BinCentroid get_from_bin(const int b) const {
           const int xBin = b % xBins.nTot;
   	   const int yBin = (b - xBin)/xBins.nTot;
           return BinCentroid(xBins.centroid(xBin), yBins.centroid(yBin));
   }
   int find_bin(const Lepton & lep) const {
      return find_bin(lep.eta, lep.phi);
   }
};


void usage() {
std::cout 
   << "Root Input units GeV based. All other outputs and inputs are TeV based (e.g. smiley perturbations are specified in perTeV and sagitas are output in perTeV).\n"
   << "\n"
   << "1st argument (mandatory) = 'tag' on basis of which other file names will be named.\n" 
   << "2nd argument (mandatory) = ROOT file to read.  If 'NONE' then will attempt to read M and K from tag.M and tag.K.\n" 
   << "3rd argument (mandatory) = max events to process (negaive means 'all')\n"
   << "4th argument (mandatory) strength factor for smiley watermark in (/TeV).  0 means add no smiley watermark.\n" 
   << " arg 5: eta bins\n"
   << " arg 6: phi bins\n"
   << " arg 7: always recalc_mll (1=true, 0=false)\n"
   << " arg 8: Attempt the bias calcuation.\n"
   << " arg 9: Leave mass scale unchanged (1=true, 0=false).\n"
   << " arg 10: safety_factor (1=100% of mZ subtractd, 0=0% subtracted).\n"
   << std::endl;
}

struct Config {
  Config(const Bins2D & bins2D) : bins2D(bins2D) {};
  std::string input_file;
  int max_events;
  double perturbation;
  std::string tag;
  bool recalc_mll;
  Bins2D bins2D;
  bool attempt_bias_calc;
  bool leave_mass_scale_fixed;
  double safety_factor;
};


#include <iostream> 
#include <vector>
#include <string>
#include <iomanip>

#include "TFile.h"
#include "TTree.h"


void fill_M_K_from_root_file(Eigen::MatrixXd & M, // output
                             Eigen::VectorXd & K, // output
                             const Config & config //input
                             ){

  const double smileyPerturbation = config.perturbation;
  const int bins = config.bins2D.bins;
  const Bins & xBins = config.bins2D.xBins;
  const Bins & yBins = config.bins2D.yBins;

  // Create some temporary storage:
  Event event; // Will be TeV based

  // Helper vectors:
  Eigen::MatrixXd H_mean_ee_mat = Eigen::MatrixXd::Zero(bins,bins);
  Eigen::VectorXd E_mean_e_vec = Eigen::VectorXd::Zero(bins);
  Eigen::VectorXd F_mean_mll2e_vec = Eigen::VectorXd::Zero(bins);
  double          g_mean_mll2 = 0;

  // ROOT STUFF
  TFile* ifile = TFile::Open(config.input_file.c_str(), "READ");
  TTree* tree = (TTree*)ifile->Get("tree");
  // Declaration of leaf types
       double        muPlusPt;
  double        muMinusPt;
  double        muPlusEta;
  double        muMinusEta;
  double        muPlusPhi;
  double        muMinusPhi;
  int           isJetInEvent;
  double        mll;
  // set branches 
  tree->SetBranchAddress("muPlusPt",   &muPlusPt);
  tree->SetBranchAddress("muMinusPt",  &muMinusPt); 
  tree->SetBranchAddress("muPlusEta",  &muPlusEta); 
  tree->SetBranchAddress("muMinusEta", &muMinusEta);
  tree->SetBranchAddress("muPlusPhi",  &muPlusPhi);
  tree->SetBranchAddress("muMinusPhi", &muMinusPhi);
  tree->SetBranchAddress("isJetInEvent", &isJetInEvent);
  tree->SetBranchAddress("mll",        &mll);




  int nentries = tree->GetEntries(); 
  std::cerr << "Root file has " << nentries << " entries" << std::endl; 
  std::cout <<"#mll2 pt+ eta+ phi+ pt- eta- phi-" << std::endl;
  std::cout << std::setprecision(9);

  long int num_events=0;
  for (int ientry=0; (ientry < nentries && (num_events<config.max_events || config.max_events<0)); ientry++) {

    tree->GetEntry(ientry);
    // Acknowledge ROOT's units for the things just read in:
    muPlusPt   *=GeV;
    muMinusPt  *=GeV; 
    muPlusEta  *=1; 
    muMinusEta *=1;
    muPlusPhi  *=1;
    muMinusPhi *=1;
    //isJetInEvent;
    mll        *=GeV;
    // Acknowledge that we read in the event:
    ++num_events;



    /////////////////////////////////////////////////////////// 
    // Absorb the event into the event structure:
    ///////////////////////////////////////////////////////////
    event.mll2 = mll*mll;
    // mu+ 
    event.leps[0].charge = +1;
    event.leps[0].pT = muPlusPt;
    event.leps[0].eta = muPlusEta;
    event.leps[0].phi = muPlusPhi;
    // mu-
    event.leps[1].charge = -1;
    event.leps[1].pT = muMinusPt;
    event.leps[1].eta = muMinusEta;
    event.leps[1].phi = muMinusPhi;

    // Make corrections to event if nececssary:
    static int hit=1;
    static int not_hit=1;
    if (smileyPerturbation!=0) {
        for (Lepton & lep : event.leps) {
        //int lep_index=0; lep_index<2; ++lep_index) {
	//        const Lepton & lep = event.leps[lep_index];

                const double x = xBins.unit_frac(lep.eta);
                const double y = yBins.unit_frac(lep.phi);
                const double smil = unit_smiley_first_quadrant(x,y);
                if (smil) {
            	    ++hit;
                } else {
            	    ++not_hit;
                }
                //const double sWeight = smil ? 1 : -static_cast<double>(hit)/static_cast<double>(not_hit); // For mean smiley compensation
                const double sWeight = smil;
                if (sWeight) {
                     const double nomSag = 1./lep.pT;
            	     const double newSag = nomSag + (lep.charge)*smileyPerturbation*sWeight;
            	     if (newSag>nomSag*0.5) lep.pT = 1./newSag; // Protect against charge inversion
                }
        }
    }
    if (config.recalc_mll || smileyPerturbation!=0) {
	    event.mll2 = (event.leps[0].lvec() + event.leps[1].lvec()).m2();
    }

    /////////////////////////////////////////////////////////// 
    // The event is now completely set up
    ///////////////////////////////////////////////////////////

    

    // Now do some record keeping ... 
    const double safetified_value_of_mll2 = ( 
		     (config.safety_factor) ? // Improves numerical precision of k calc. Not to be used in the M calc.  So only use in g_mean_mll2 and F_mean_mll2e_vec
		     (event.mll2 - (Z_mass*Z_mass) * config.safety_factor) :
		     (event.mll2) 
		   );


    g_mean_mll2 += safetified_value_of_mll2;
    for (const Lepton & lepS : event.leps) {
       const double thingS = lepS.new_thing() * event.mll2;
       const int binS = config.bins2D.find_bin(lepS);
       E_mean_e_vec(binS) += thingS;
       F_mean_mll2e_vec(binS) += safetified_value_of_mll2 * thingS;
       for (const Lepton & lepT : event.leps) {
           const double thingT = lepT.new_thing() * event.mll2;
	   const int binT = config.bins2D.find_bin(lepT);
	   H_mean_ee_mat(binS, binT) += thingS * thingT;
       } 
    }
  }
  ifile->Close();
  std::cerr << "Read " << num_events << " events." << std::endl;

  // Tidy up normalisations now that we know how many events there were:
  std::cerr << "Normalising ... " << std::endl;
  g_mean_mll2 /= static_cast<double>(num_events);
  E_mean_e_vec /= static_cast<double>(num_events);
  F_mean_mll2e_vec /= static_cast<double>(num_events);
  H_mean_ee_mat /= static_cast<double>(num_events);
  std::cerr << "                ... done." << std::endl;

  // Now finish the computations:
  std::cerr << "Making K ... " << std::endl;
  Eigen::VectorXd tmpK = F_mean_mll2e_vec - g_mean_mll2 * E_mean_e_vec;
  std::cerr << "                ... done." << std::endl;
  std::cerr << "Making M ... " << std::endl;
  Eigen::MatrixXd tmpM = H_mean_ee_mat - E_mean_e_vec * E_mean_e_vec.transpose(); 
  std::cerr << "                ... done." << std::endl;

  const double veto = (config.leave_mass_scale_fixed ? 1 : 0);

  K = Eigen::VectorXd(bins+1);  // The "+1" supports a Lagrange Multuplier constraint for mass fixing.
  K << tmpK, 0; // the 0 assists with the LM constraint term.
  
  M = Eigen::MatrixXd(bins+1,bins+1); // The "+1" supports a Lagrange Multuplier constraint.
  M << tmpM, veto*E_mean_e_vec,
       veto*(E_mean_e_vec.transpose()), 0;

  // Note that in the case that we leave the mass cale free, then M is zero in its last row and last column, and there is therefore no constraint on the lagrange multiplier. But that's fine, as the LM doesn't need to be there if we are not interested in fixing the mass scale!
  

} // end of iterations 


void example_block_matrix_construction() {
    Eigen::MatrixXd m(2,2); // Must specify size here or next line will not work.
    m << 1, 2,
         3, 4;

    Eigen::VectorXd v(2);
    v << 23, 24;
    
    Eigen::MatrixXd M(3,3); // Must specify size here or next line will not work.
    M << m,             v,
         v.transpose(), 100;

    std::cout << M << std::endl;

    Eigen::VectorXd V(3);
    V << v,
         -10;
    std::cout << V << std::endl;
}


int main(int narg, char * args[]) {

  // example_block_matrix_construction();
  // return 0;

  if (narg!=10+1) {
     usage();
     exit(1);
  }

  const int etaBins = atoi(args[5]);
  const int phiBins = atoi(args[6]);
  const Bins xBins(etaBins, -2.47, 2.47, false, false); // don't need overflow bins unless making etamax less than 2.47
  const Bins yBins(phiBins, -1.00001*M_PI, +1.00001*M_PI, false, false);
  Config config(Bins2D(xBins, yBins));
  
  config.input_file = args[2];
  config.max_events = atoi(args[3]);
  config.perturbation = atof(args[4])*TeV; // Note the input is supplied on the command line in TeV
  config.tag = args[1];
  config.recalc_mll = atoi(args[7]);
  config.attempt_bias_calc = atoi(args[8]);
  config.leave_mass_scale_fixed = atoi(args[9]);
  config.safety_factor = atof(args[10]);


  {
      std::cerr << "Outputting metadata" << std::endl;
      std::ofstream of(config.tag+"/metadata");
      of << config.bins2D.histAxisSpec() << std::endl;
      of.close();
  }

  std::cerr << "Creating M and K references" << std::endl;
  // Desired outputs:
  Eigen::MatrixXd M;
  Eigen::VectorXd K;


  fill_M_K_from_root_file(M, K, config); // Smiley
  
  //std::cerr << "M matrix : " << M << std::endl;
  //std::cerr << "K vector : " << K << std::endl;

  {
      std::cerr << "Outputting M" << std::endl;
      std::ofstream of(config.tag+"/M");
      of << M << std::endl;
      of.close();
  }
  {
      std::cerr << "Outputting K" << std::endl;
      std::ofstream of(config.tag+"/K");
      of << K << std::endl;
      of.close();
  }
    

  // We now want to solve M*deltaVec =  K for the sagitta biasses in each bin (which we hold in deltaVec)
  std::cerr << "Attempting to solve M*delta = K ..." << std::endl;
  Eigen::VectorXd deltaVec = M.colPivHouseholderQr().solve(K);
  //Eigen::VectorXd deltaVec = M.completeOrthogonalDecomposition().solve(K);
  std::cerr << "                       ... done" << std::endl;

  //std::cerr << "deltaVec : " << deltaVec << std::endl;
  
  {
	// Get just the bits not to do with the Lagrange Multiplier:
       const int bins = config.bins2D.bins;

       Eigen::MatrixXd MM = M.topLeftCorner(bins,bins);
       Eigen::VectorXd KK = K.head(bins);
       Eigen::VectorXd ddelta = deltaVec.head(bins);


       // In principle, Delta (the improvement in the variance, big and positive = good) would be defined as follows:
       // const double Delta = 2.0*KK.dot(ddelta) - (ddelta.transpose())*MM*ddelta;
       // However, for the reasons given in the documentation, it may be proved that the above should be equivalent to the simpler definition:
       const double Delta = KK.dot(ddelta);

       {
           std::cerr << "Outputting stats" << std::endl;
           std::ofstream of(config.tag+"/stats");
           of << std::setprecision(10) << "Delta_VarMll2_(TeV^4) " << Delta << std::endl;
           of.close();
       }

  }

  {
      std::cerr << "Outputting biases" << std::endl;
      std::ofstream of(config.tag+"/biases");
      for (int bin=0; bin<config.bins2D.bins; ++bin) {
	  const Bins2D::BinCentroid bs = config.bins2D.get_from_bin(bin);
          of << bs.eta << " " << bs.phi << " " << deltaVec[bin] << std::endl;
      }
      of.close();
  }



  return 0;
}




