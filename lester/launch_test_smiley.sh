#!/bin/sh

for i in `seq -w 0 39`
do {
export FRAC=`echo $i/39*0.4 | bc -l` 

export TAGX=data15_fixedScale_with_sagCorr_all_40x40_smileyfrac
export TAG=${TAGX}_$i

cat << EOF | sed 's/XX/'$FRAC'/g' > $TAG.config
/r10/atlas/emus/histograms/data28Aug_2008111409_merged/data15.muons.root # With sagitta corrections
-1     # max events
XX     # watermark strength
40     # etabins
40     # phiBins
1      # recalc mll
1      # try to compute biases
1      # fix mass scale (1=yes, 0=no)
1      # make var calc safe (1=yes, 0=no)
EOF

echo $TAG                  >> ${TAGX}_ALL_DIRS
echo $TAG/plot_std.png     >> ${TAGX}_ALL_PLOTS
echo $TAG/plot_auto.png    >> ${TAGX}_ALL_PLOTS
echo $TAG/plot_stdRain.png >> ${TAGX}_ALL_PLOTS
echo $TAG/plot_wide.png    >> ${TAGX}_ALL_PLOTS


} done

echo consider 
echo 'convert -delay 20 -loop 0 '$TAGX'_*/plot_std.png all.gif'
