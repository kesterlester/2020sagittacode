#!/bin/sh

for i in `seq -w 0 19`
do {

cat << EOF | sed 's/XX/'$i'/g' > data15_fixedScale_with_sagCorr_all_12x12_unsafe_$i.config
/r10/atlas/emus/histograms/data28Aug_2008111409_merged/data15.muons.root # With sagitta corrections
9999XX # max events
0.0    # watermark strength
12     # etabins
12     # phiBins
1      # recalc mll
1      # try to compute biases
1      # fix mass scale (1=yes, 0=no)
0      # make var calc safe (1=yes, 0=no)
EOF

echo data15_fixedScale_with_sagCorr_all_12x12_unsafe_$i                  >> data15_fixedScale_with_sagCorr_all_12x12_unsafe_ALL_DIRS
echo data15_fixedScale_with_sagCorr_all_12x12_unsafe_$i/plot_std.png     >> data15_fixedScale_with_sagCorr_all_12x12_unsafe_ALL_PLOTS
echo data15_fixedScale_with_sagCorr_all_12x12_unsafe_$i/plot_auto.png    >> data15_fixedScale_with_sagCorr_all_12x12_unsafe_ALL_PLOTS
echo data15_fixedScale_with_sagCorr_all_12x12_unsafe_$i/plot_stdRain.png >> data15_fixedScale_with_sagCorr_all_12x12_unsafe_ALL_PLOTS
echo data15_fixedScale_with_sagCorr_all_12x12_unsafe_$i/plot_wide.png    >> data15_fixedScale_with_sagCorr_all_12x12_unsafe_ALL_PLOTS


} done
