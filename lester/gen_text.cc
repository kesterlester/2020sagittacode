#include <iostream> 
#include <vector>
#include <string>
#include <iomanip>

#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"


// forward declarations 
//double calc_sagitta(int charge, float mll2, float mZ2, float pt, double old_sagitta);
//TH2D* combine_averages(const Hist& h1, const Hist& h2);


void scan(int narg, char*args[]){

  const float mZ = 91.187; // all units are in GeV 
  float mZ2 = mZ*mZ; 

  // open file with muons 
  const std::string old_input_file = "/r10/atlas/emus/histograms/data17Aug_withMuons_2008111409_merged/data15.muons.root";
  if (narg!=2) {
     std::cerr << "Must name input file as argument." << std::endl;
     exit(10);
  }
const std::string input_file = args[1];


  TFile* ifile = TFile::Open(input_file.c_str(), "READ");
  TTree* tree = (TTree*)ifile->Get("tree");

  // Declaration of leaf types
       double        muPlusPt;
  double        muMinusPt;
  double        muPlusEta;
  double        muMinusEta;
  double        muPlusPhi;
  double        muMinusPhi;
  int           isJetInEvent;
  double        mll;

  // set branches 
       tree->SetBranchAddress("muPlusPt",   &muPlusPt);
  tree->SetBranchAddress("muMinusPt",  &muMinusPt); 
  tree->SetBranchAddress("muPlusEta",  &muPlusEta); 
  tree->SetBranchAddress("muMinusEta", &muMinusEta);
  tree->SetBranchAddress("muPlusPhi",  &muPlusPhi);
  tree->SetBranchAddress("muMinusPhi", &muMinusPhi);
  tree->SetBranchAddress("isJetInEvent", &isJetInEvent);
  tree->SetBranchAddress("mll",        &mll);

  // Hist objects to store the sagitta results
       // eta
            int xbins  = 50;
  float xmin = -2.4;
  float xmax = 2.4;
  // phi  
       int ybins  = 50;
  float ymin = -1*M_PI;
  float ymax = M_PI;

  int nentries = tree->GetEntries(); 
  std::cerr << "Will loop over " << nentries << " entries" << std::endl; 
  std::cout <<"#mll2 pt+ eta+ phi+ pt- eta- phi-" << std::endl;
  std::cout << std::setprecision(9);
  for (int ientry=0; ientry < nentries; ientry++) {
    tree->GetEntry(ientry);

    float mll2=mll*mll;

    std::cout << mll2 << " ";

    // mu+ 
    float pt_plus = muPlusPt;
    float eta_plus = muPlusEta;
    float phi_plus = muPlusPhi;

    std::cout << pt_plus << " ";
    std::cout << eta_plus << " ";
    std::cout << phi_plus << " ";


    // mu-
    float pt_minus = muMinusPt;
    float eta_minus = muMinusEta;
    float phi_minus = muMinusPhi;

    std::cout << pt_minus << " ";
    std::cout << eta_minus << " ";
    std::cout << phi_minus << "\n";


  }

  ifile->Close();

} // end of iterations 

int main(int narg, char*args[]){
  scan(narg, args);
  return 0;
}

