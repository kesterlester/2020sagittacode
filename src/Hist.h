#include <iostream>
#include <TH2D.h>
#include <memory>

// Generate random sring, useful to get over ROOT nonsense; 
std::string RandomString(){
  int len  =6; // good enough 
  std::string str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  std::string newstr;
  int pos;
  while(newstr.size() != len) {
    pos = ((rand() % (str.size() - 1)));
    newstr += str.substr(pos,1);
  }
  return newstr;
}

// Object to store sagittas 
class Hist{
  private:
    std::string name;

  public:

    // We want this on the stack, why not 
    TH2D value_hist;
    TH2D count_hist; 

    explicit Hist(int xbins, float xmin, float xmax, int ybins, float ymin, float ymax){
      name = RandomString(); 
      std::string val_name   = "values_"+name;
      std::string count_name = "counts_"+name;
      /*std::cout << val_name << " " << count_name << std::endl;*/
      
      value_hist = TH2D(val_name.c_str(), "", xbins, xmin, xmax, ybins, ymin, ymax);
      value_hist.SetDirectory(0); // take ownership
      count_hist = TH2D(count_name.c_str(), "", xbins, xmin, xmax, ybins, ymin, ymax);
      count_hist.SetDirectory(0);
    }

    void fill(float xpos, float ypos, double val){
      value_hist.Fill(xpos, ypos, val);
      count_hist.Fill(xpos, ypos, 1);
    }

    std::string get_name() const { return name; }
    /*~Hist(){*/
    /*std::cout << "calling destructor for " << name << std::endl; */
    /*}*/

};
