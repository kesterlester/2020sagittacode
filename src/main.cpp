#include <iostream> 
#include <vector>
#include <string>

#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"

#include "Hist.h"

// forward declarations 
TH2D* run_iteration(int, TH2D*); 
double calc_sagitta(int, double, float, double);
TH2D* combine_averages(const Hist& h1, const Hist& h2);

int main(){

  std::cout << "Hello world!" << std::endl;

  TFile* ofile = TFile::Open("results.root", "RECREATE");

  TH2D* sagitta_averages = NULL;
  int n_iterations = 200;
  for(int iiter=0; iiter<n_iterations; ++iiter){
    sagitta_averages = run_iteration(iiter, sagitta_averages);
    std::cout << "Completed iteration: " << iiter+1 << std::endl;

    ofile->cd();
    sagitta_averages->SetName( ("average_sagitta_"+std::to_string(iiter+1)).c_str() );
    sagitta_averages->Write();

  }
  ofile->Close(); 

  std::cout << "End of iterations" << std::endl;
  return 0;

}


TH2D* run_iteration(int iiter, TH2D* sagitta_averages){

  //const float mZ = 91.187; // all units are in GeV 
  //float mZ2 = mZ*mZ; 

  // open file with muons 
  std::string input_file = "/r10/atlas/emus/histograms/data17Aug_withMuons_2008111409_merged/data15.muons.root";
  TFile* ifile = TFile::Open(input_file.c_str(), "READ");
  TTree* tree = (TTree*)ifile->Get("tree");

  // Declaration of leaf types
  double        muPlusPt;
  double        muMinusPt;
  double        muPlusEta;
  double        muMinusEta;
  double        muPlusPhi;
  double        muMinusPhi;
  int           isJetInEvent;
  double        mll;

  // set branches 
  tree->SetBranchAddress("muPlusPt",   &muPlusPt);
  tree->SetBranchAddress("muMinusPt",  &muMinusPt); 
  tree->SetBranchAddress("muPlusEta",  &muPlusEta); 
  tree->SetBranchAddress("muMinusEta", &muMinusEta);
  tree->SetBranchAddress("muPlusPhi",  &muPlusPhi);
  tree->SetBranchAddress("muMinusPhi", &muMinusPhi);
  tree->SetBranchAddress("isJetInEvent", &isJetInEvent);
  tree->SetBranchAddress("mll",        &mll);

  // Hist objects to store the sagitta results
  // eta
  int xbins  = 50;
  float xmin = -2.4;
  float xmax = 2.4;
  // phi  
  int ybins  = 50;
  float ymin = -1*M_PI;
  float ymax = M_PI;
  Hist minus_hist(xbins, xmin, xmax, ybins, ymin, ymax);  
  Hist plus_hist( xbins, xmin, xmax, ybins, ymin, ymax);  

  int nentries = tree->GetEntries(); 
  if(iiter==0) std::cout << "Will loop over " << nentries << " entries" << std::endl; 
  for (int ientry=0; ientry < nentries; ientry++) {
    tree->GetEntry(ientry);

    //float mll2(0);
    //double DeltaM2(0.0);
    double frac(0.0);

    // mu+ 
    float pt_plus = muPlusPt;
    float eta_plus = muPlusEta;
    float phi_plus = muPlusPhi;
    double old_sagitta_plus = 0.0;
    if(iiter!=0){
      // Extract the saggita average from the previous iteration
      int temp_xbin = sagitta_averages->GetXaxis()->FindBin(eta_plus);
      int temp_ybin = sagitta_averages->GetYaxis()->FindBin(phi_plus);
      old_sagitta_plus = sagitta_averages->GetBinContent(temp_xbin, temp_ybin);
    }


    // mu-
    float pt_minus = muMinusPt;
    float eta_minus = muMinusEta;
    float phi_minus = muMinusPhi;
    double old_sagitta_minus = 0.0;
    if(iiter!=0){
      // Extract the saggita average from the previous iteration
      int temp_xbin = sagitta_averages->GetXaxis()->FindBin(eta_minus);
      int temp_ybin = sagitta_averages->GetYaxis()->FindBin(phi_minus);
      old_sagitta_minus = sagitta_averages->GetBinContent(temp_xbin, temp_ybin);
    }

    if(iiter==0){
      //mll2 = mll*mll;
      frac = 0.001; 
    }
    else{
      // Subsequent iterations, recalculate 
      //mll2 = mZ2 + mZ2*( pt_plus*old_sagitta_plus - pt_minus*old_sagitta_minus);
      frac = (pt_plus*old_sagitta_plus - pt_minus*old_sagitta_minus);
    }

    // Calculate the sagittas for this event 
    double sagitta_plus  = calc_sagitta( 1, frac, pt_plus,  old_sagitta_plus);
    double sagitta_minus = calc_sagitta(-1, frac, pt_minus, old_sagitta_minus);

    plus_hist.fill( eta_plus,  phi_plus,  sagitta_plus);
    minus_hist.fill(eta_minus, phi_minus, sagitta_minus);

    //simple_sagitta_plus.Fill(sagitta_plus);
    //simple_sagitta_minus.Fill(sagitta_minus);
  }

  ifile->Close();
  std::vector<Hist> results;

  TH2D* new_sagitta_averages = combine_averages(minus_hist, plus_hist);
  return new_sagitta_averages;

} // end of iterations 

TH2D* combine_averages(const Hist& h1, const Hist& h2){

  bool debug(false);
  if(debug){
    h1.value_hist.Print();
    h2.value_hist.Print();
  }

  TH2D* average_hist = static_cast<TH2D*>(h1.value_hist.Clone( (std::string(h1.value_hist.GetName())+"_clone").c_str() )); 
  average_hist->Add( &h2.value_hist );

  TH2D* total_counts = static_cast<TH2D*>(h1.count_hist.Clone( (std::string(h1.count_hist.GetName())+"_clone").c_str() ));
  total_counts->Add( &h2.count_hist );

  // Get total counts in all bins
  float grand_total = total_counts->Integral();  // should be an int 

  //average_hist->Divide(total_counts);
  for(int xbin=0; xbin < average_hist->GetNbinsX()+2; ++xbin){
    for(int ybin=0; ybin < average_hist->GetNbinsY()+2; ++ybin){
      average_hist->SetBinContent(xbin,  ybin,  average_hist->GetBinContent(xbin, ybin)/grand_total);
    }
  }


  if(debug) average_hist->Print();
  return average_hist; 
}

double calc_sagitta(int charge, double frac, float pt, double old_sagitta){
  return (-1*charge * 0.5*frac * (1 + charge * pt * old_sagitta) / pt)  + old_sagitta;
}
