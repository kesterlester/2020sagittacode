#!/usr/bin/python
'''
Script to make plots of average muon pT, split by charge, as a functions of phi and eta
'''
from ROOT import * 
from utils.Sample import Sample 
import math

gROOT.SetBatch(1)
gStyle.SetOptStat(0)

from EmuStyle import SetEmuStyle
SetEmuStyle()

#____________________________________________________________________________
def make_two_pads():
  '''Make and return upper and  lower pads for ratio plots
  '''
  pad1 = TPad("upper_pad","upper_pad",0.0,0.5,1.0,1.0,21)
  pad2 = TPad("lower_pad","lower_pad",0.0,0.0,1.0,0.5,22)
  pad1.Draw()
  pad1.SetRightMargin(0.04)
  pad2.SetRightMargin(0.04)
  pad2.Draw()
  pad1.SetFillColor(kWhite)
  pad2.SetFillColor(kWhite)
  pad1.SetTopMargin(0.02)
  pad1.SetBottomMargin(0.01)
  pad2.SetTopMargin(0.05)
  pad2.SetBottomMargin(0.4)
  pad1.SetLeftMargin(0.1)
  pad2.SetLeftMargin(0.1)
  pad1.cd()
  return pad1, pad2

def main(debug):

   make_profiles("data", "Eta")
   make_profiles("data", "Phi")

def make_profiles(sample, variable):

   
   # General plot settings  
   plus_colour = 2
   minus_colour = 4
   ytitle_offset = 0.75 

   # Create the pads 
   can = TCanvas("can", "can", 500, 500)
   pad1, pad2 = make_two_pads()
   pad1.cd()

   # Extract the plots 
   data = Sample(sample, isData=True)
   path = "fullCutflow/2mu/OSmumu/zpeak/OSmumu_PTight_MTight/OSmumu_PTight_MTight/"

   plus_path = path + "muPlusPt"+variable
   minus_path = path + "muMinusPt"+variable
   hist_muPlus = data.get_histogram(plus_path)
   hist_muMinus = data.get_histogram(minus_path)

   counts = hist_muPlus.Integral()
   if variable  == "Eta":
      hist_muPlus.Rebin2D(1, 5)
      hist_muMinus.Rebin2D(1, 5)

   # Profile 
   prof_muPlus = hist_muPlus.ProfileY()
   prof_muMinus = hist_muMinus.ProfileY()

   
   # Plot styling 
   prof_muMinus.SetMarkerColor(minus_colour)
   prof_muMinus.SetLineColor(minus_colour)
   prof_muMinus.SetMarkerSize(0.5)

   prof_muPlus.SetMarkerColor(plus_colour)
   prof_muPlus.SetLineColor(plus_colour)
   prof_muPlus.SetMarkerSize(0.5)

   prof_muMinus.Draw()
   yaxis = prof_muMinus.GetYaxis()
   xaxis = prof_muMinus.GetXaxis()


   yaxis.SetTitle("#LTp_{T}#GT [GeV]")
   yaxis.SetRangeUser(38, 46)
   yaxis.SetTitleOffset(0.75)
   prof_muPlus.Draw("same")
   prof_muPlus.SetMarkerStyle(22)

   leg = TLegend(0.2, 0.75, 0.45, 0.95)
   leg.SetTextFont(42)
   leg.AddEntry(prof_muPlus, "#mu^{+} : "+" {0:.1f}".format(counts), "lp")
   leg.AddEntry(prof_muMinus,"#mu^{#minus}: "+" {0:.1f}".format(counts), "lp")
   leg.SetFillStyle(0)
   #leg.SetNColumns(2)
   leg.SetBorderSize(0)
   leg.Draw()

   # Make the ratio
   pad2.cd()
   ratio = make_ratio(prof_muPlus, prof_muMinus)
   #ratio = prof_muPlus.Clone()
   #ratio.Divide(prof_muMinus)
   ratio.Draw()

   r_xaxis = ratio.GetXaxis()
   r_yaxis = ratio.GetYaxis()
   r_xaxis.SetTitle(variable)
   r_yaxis.SetRangeUser(0.95, 1.05)
   #r_yaxis.SetTitleOffset(-0.001)
   r_yaxis.SetTitleOffset(0.75)
   line = TLine(-2.5, 1, 2.5, 1)
   line.Draw()
   r_yaxis.SetTitle("#mu^{+} / #mu^{#minus}")
   if variable == "Eta":
      xaxis.SetRangeUser(-2.5, 2.5)
      r_xaxis.SetRangeUser(-2.5, 2.5)

   can.SaveAs("sagitta_{0}_{1}.pdf".format(sample, variable))


def make_ratio(numerator, denominator):

   nbins   = numerator.GetNbinsX()
   low_bin = numerator.GetBinLowEdge(1)
   up_bin  = numerator.GetBinLowEdge(nbins) + numerator.GetBinWidth(1)

   hist = TH1D("hist","hist", nbins, low_bin, up_bin)

   for ibin in range(0,  nbins+1):

      num = numerator.GetBinContent(ibin)
      error_up =  numerator.GetBinError(ibin)
      denom = denominator.GetBinContent(ibin)
      error_down = denominator.GetBinError(ibin)

      try:
         ratio  = num / denom
      except ZeroDivisionError:
         ratio = 0

      if  ratio == 0:
         ratio_error = 0
      else:
         ratio_error = math.sqrt( 
               ratio**2 * ( (error_up**2/ num**2) + (error_down**2/denom**2) )
               )

      hist.SetBinContent(ibin, ratio)
      hist.SetBinError(ibin, ratio_error)
      print num, error_up, '\t', denom, error_down
      print ratio,  ratio_error

   return hist 

if __name__ == "__main__":

   import argparse
   parser = argparse.ArgumentParser()
   parser.add_argument("-d", "--debug", help="Turn on debug messages", action="store_true", default=False)
   args = parser.parse_args()
   debug = args.debug

   main(debug)
